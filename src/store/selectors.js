export const artifactValidSelector = ({artifact}) => artifact.valid;
export const artifactSelector = ({artifact}) => artifact;
export const artifactRemainingRollsSelector = ({artifact}) => artifact.remainingRolls;
export const artifactTierSelector = ({artifact}) => artifact.tier;
export const starsSelector = ({artifact}) => artifact.stars;
export const levelSelector = ({artifact}) => artifact.level;
export const statsSelector = ({artifact}) => artifact.stats;
export const statsNameSelector = ({artifact}) => {
    var res = [];
    artifact.stats.forEach(element => {
        res.push(element.name);
    });
    return res;
};
export const artifactGoodRollsSelector = ({artifact}) => {
    var res = 0;
    artifact.stats.forEach(e => {
        if (e.useful && e.valid){
            res += e.rolls.length
        }
    })
    return res
}
export const artifactBadRollsSelector = ({artifact}) => {
    var res = 0;
    artifact.stats.forEach(e => {
        if (!e.useful && e.valid){
            res += e.rolls.length
        }
    })
    return res
}