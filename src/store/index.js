import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware  from 'redux-saga';
import Reducers from '../reducers/index';
import Sagas from '../sagas/index';

const sagaMiddleware = createSagaMiddleware();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(Reducers, composeEnhancers(applyMiddleware(sagaMiddleware)));

sagaMiddleware.run(Sagas);

export default store;