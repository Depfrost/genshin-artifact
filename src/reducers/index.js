import { combineReducers } from 'redux';
import artifactReducer from './artifact'

export default combineReducers({
    artifact: artifactReducer
});