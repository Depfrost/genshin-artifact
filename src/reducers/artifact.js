import * as constants from '../constants/artifact'

const initialState = {
    stars: 4,
    level: 0,
    valid: false,
    remainingRolls: 10,
    tier: 'A',
    stats: []
}

const artifactReducer = (state = initialState, action) => {
    switch(action.type) {
        case constants.SET_STARS_ARTIFACT:
            return {
                ...state,
                stars: action.payload.stars
            };
        case constants.SET_LEVEL_ARTIFACT:
            return {
                ...state,
                level: action.payload.level
            };
        case constants.SET_STATS_ARTIFACT:
            var new_stats = state.stats.filter(element => {
                var index = action.payload.stats.indexOf(element.name);
                if (index !== -1) {
                    action.payload.stats.splice(index, 1);
                    return true;
                }
                return false;
            });
            action.payload.stats.forEach(element => {
                new_stats.push({
                    name: element,
                    value: 0,
                    useful: true,
                    valid: false,
                    tier: "C",
                    rolls: []
                });
            });
            return {
                ...state,
                stats: new_stats
            };
        case constants.SET_STAT_VALUE_ARTIFACT:
            return {
                ...state,
                stats: state.stats.map(
                    (e) => {
                        if (e.name === action.payload.statName){
                            return {...e, value: action.payload.statValue}
                        }
                        return e;
                    }
                )
            };
        case constants.SET_STAT_USEFUL_ARTIFACT:
            return {
                ...state,
                stats: state.stats.map(
                    (e) => {
                        if (e.name === action.payload.statName){
                            return {...e, useful: action.payload.useful}
                        }
                        return e;
                    }
                )
            };
        case constants.REQUEST_PROCESS_ARTIFACT_SUCCESS:
            return action.payload;
        case constants.REQUEST_OCR_ARTIFACT_SUCCESS:
            return {
                ...state,
                level: action.payload.level,
                stats: action.payload.stats
            }
        default:
            return state
    }
}

export default artifactReducer;