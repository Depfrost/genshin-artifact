import { connect } from 'react-redux'
import ArtifactTier from '../components/artifactTier'
import { artifactTierSelector } from '../store/selectors'


const mapStateToProps = (state, ownProps) => ({
    artifactTier: artifactTierSelector(state)
})

export default connect(mapStateToProps)(ArtifactTier)