import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { requestOCR } from '../actions/artifact'
import ImagePicker from '../components/imagePicker'


const mapStateToProps = (state, ownProps) => ({
})

const mapDispatchToProps = dispatch => {
    return {
        requestOCR: bindActionCreators(requestOCR, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ImagePicker)