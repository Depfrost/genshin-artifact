import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { setStatValueArtifact, setStatUsefulArtifact } from '../actions/artifact'
import StatValuesPicker from '../components/statValuesPicker'
import { STATS } from '../constants/artifact';
import { statsSelector } from '../store/selectors'

const statsToDisplayStats = (stats) => {
    return stats.map((elem) => { return {valid: elem.valid, value: "value" in elem ? elem.value : 0, useful: elem.useful, name: STATS.find(e => e.name === elem.name).display_name}});
}

const mapStateToProps = (state, ownProps) => ({
    stats: statsToDisplayStats(statsSelector(state))
})

const mapDispatchToProps = dispatch => {
    return {
        setStatValue: bindActionCreators(setStatValueArtifact, dispatch),
        setStatUseful: bindActionCreators(setStatUsefulArtifact, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StatValuesPicker)