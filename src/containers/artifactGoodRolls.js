import { connect } from 'react-redux'
import ArtifactGoodRolls from '../components/artifactGoodRolls'
import { artifactGoodRollsSelector } from '../store/selectors'

const mapStateToProps = (state, ownProps) => ({
    numberOfGoodRolls: artifactGoodRollsSelector(state)
})

export default connect(mapStateToProps)(ArtifactGoodRolls)