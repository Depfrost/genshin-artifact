import { connect } from 'react-redux'
import ArtifactBadRolls from '../components/artifactBadRolls'
import { artifactBadRollsSelector } from '../store/selectors'


const mapStateToProps = (state, ownProps) => ({
    numberOfBadRolls: artifactBadRollsSelector(state)
})

export default connect(mapStateToProps)(ArtifactBadRolls)