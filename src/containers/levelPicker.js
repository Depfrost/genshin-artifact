import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { setLevelArtifact } from '../actions/artifact'
import LevelPicker from '../components/levelPicker'
import { levelSelector } from '../store/selectors'


const mapStateToProps = (state, ownProps) => ({
    level: Math.floor(levelSelector(state)/4)*4
})

const mapDispatchToProps = dispatch => {
    return {
        setLevel: bindActionCreators(setLevelArtifact, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LevelPicker)