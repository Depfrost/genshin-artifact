import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { setStatsArtifact } from '../actions/artifact'
import StatPicker from '../components/statPicker'
import { STATS } from '../constants/artifact';
import { statsNameSelector } from '../store/selectors'

const statsToDisplayStats = (stats) => {
    return stats.map(elem => STATS.find(e => e.name === elem).display_name);
}

const mapStateToProps = (state, ownProps) => ({
    stats: statsToDisplayStats(statsNameSelector(state))
})

const mapDispatchToProps = dispatch => {
    return {
        setStats: bindActionCreators(setStatsArtifact, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StatPicker)