import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { setStarsArtifact } from '../actions/artifact'
import StarPicker from '../components/starPicker'
import { starsSelector } from '../store/selectors'


const mapStateToProps = (state, ownProps) => ({
    stars: starsSelector(state)
})

const mapDispatchToProps = dispatch => {
    return {
        setStars: bindActionCreators(setStarsArtifact, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StarPicker)