import React from 'react'
import ArtifactResume from './artifactResume'
import ArtifactSelectorContainer from './artifactSelectorContainer'
import styles from '../styles/app.module.scss'

class Application extends React.Component {
    render() {
        return(
            <div className={styles.app}>
                <ArtifactSelectorContainer/>
                <ArtifactResume/>
                <br/>
                <h3 className={styles.copyrights}>GenshinArtifact ©2020 Created by David GRONDIN</h3>
            </div>
        )
    }
}

export default Application