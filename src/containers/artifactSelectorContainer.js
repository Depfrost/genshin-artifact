import { connect } from 'react-redux'
import { starsSelector } from '../store/selectors'
import StarPicker from './starPicker'
import LevelPicker from './levelPicker'
import StatPicker from './statPicker'
import StatValuesPicker from './statValuesPicker'
import ArtifactselectorContainer from '../components/artifactSelectorContainer'
import ImagePicker from './imagePicker'

const mapStateToProps = (state, ownProps) => ({
    stars: starsSelector(state),
    imagePicker: ImagePicker,
    starPicker: StarPicker,
    levelPicker: LevelPicker,
    statPicker: StatPicker,
    statValuesPicker: StatValuesPicker 
})

export default connect(mapStateToProps)(ArtifactselectorContainer)