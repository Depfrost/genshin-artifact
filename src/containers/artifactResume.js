import { connect } from 'react-redux'
import ArtifactResume from '../components/artifactResume'
import { statsSelector, artifactValidSelector } from '../store/selectors'
import { STATS } from '../constants/artifact'
import ArtifactTier from './artifactTier'
import ArtifactGoodRolls from './artifactGoodRolls'
import ArtifactBadRolls from './artifactBadRolls'
import ArtifactRemainingRolls from './artifactRemainingRolls'

const statsToDisplayStats = (stats) => {
    return stats.map((elem) => { return {...elem, name: STATS.find(e => e.name === elem.name).display_name}});
}

const mapStateToProps = (state, ownProps) => ({
    artifactValid: artifactValidSelector(state),
    stats: statsToDisplayStats(statsSelector(state)),
    artifactTier: ArtifactTier,
    artifactGoodRolls : ArtifactGoodRolls,
    artifactBadRolls : ArtifactBadRolls,
    artifactRemainingRolls : ArtifactRemainingRolls,
})

export default connect(mapStateToProps)(ArtifactResume)