import { connect } from 'react-redux'
import ArtifactRemainingRolls from '../components/artifactRemainingRolls'
import { artifactRemainingRollsSelector } from '../store/selectors'


const mapStateToProps = (state, ownProps) => ({
    numberOfRemainingRolls: artifactRemainingRollsSelector(state)
})

export default connect(mapStateToProps)(ArtifactRemainingRolls)