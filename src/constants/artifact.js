export const SET_STARS_ARTIFACT = 'SET_STARS_ARTIFACT'
export const SET_LEVEL_ARTIFACT = 'SET_LEVEL_ARTIFACT'
export const SET_STATS_ARTIFACT = 'SET_STATS_ARTIFACT'
export const SET_STAT_USEFUL_ARTIFACT = 'SET_STAT_USEFUL_ARTIFACT'
export const SET_STAT_VALUE_ARTIFACT = 'SET_STAT_VALUE_ARTIFACT'
export const REQUEST_PROCESS_ARTIFACT = 'REQUEST_PROCESS_ARTIFACT'
export const REQUEST_PROCESS_ARTIFACT_SUCCESS = 'REQUEST_PROCESS_ARTIFACT_SUCCESS'
export const REQUEST_OCR_ARTIFACT = 'REQUEST_OCR_ARTIFACT'
export const REQUEST_OCR_ARTIFACT_SUCCESS = 'REQUEST_OCR_ARTIFACT_SUCCESS'

export const STATS = [
    {name:'def_flat', display_name: 'DEF'},
    {name:'hp_flat', display_name: 'HP'},
    {name:'atk_flat', display_name: 'ATK'},
    {name:'elemental_mastery', display_name: 'Elemental Mastery'},
    {name:'def_percent', display_name: 'DEF %'},
    {name:'hp_percent', display_name: 'HP %'},
    {name:'atk_percent', display_name: 'ATK %'},
    {name:'energy_recharge', display_name: 'Energy Recharge'},
    {name:'crit_rate', display_name: 'CRIT Rate'},
    {name:'crit_damage', display_name: 'CRIT DMG'}
];