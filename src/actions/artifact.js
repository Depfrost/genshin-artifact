import { SET_STARS_ARTIFACT, SET_LEVEL_ARTIFACT, SET_STATS_ARTIFACT, SET_STAT_VALUE_ARTIFACT, SET_STAT_USEFUL_ARTIFACT, REQUEST_PROCESS_ARTIFACT, REQUEST_OCR_ARTIFACT } from '../constants/artifact'
import { STATS } from '../constants/artifact';

export const requestOCR = (image) => ({type: REQUEST_OCR_ARTIFACT, payload: { image: image }})
export const processArtifact = (artifact) => ({type: REQUEST_PROCESS_ARTIFACT, payload: { artifact: artifact }})
export const setStarsArtifact = (stars) => ({type: SET_STARS_ARTIFACT, payload: { stars:stars }})
export const setLevelArtifact = (level) => ({type: SET_LEVEL_ARTIFACT, payload: { level:level }})
export const setStatsArtifact = (stats_display) => {
    var stats = stats_display.map(element => STATS.find(elem => elem.display_name === element).name);
    return {type: SET_STATS_ARTIFACT, payload: { stats:stats }}
}
export const setStatValueArtifact = (stat, statValue) => {
    var statName = STATS.find(elem => elem.display_name === stat).name;
    return {type: SET_STAT_VALUE_ARTIFACT, payload: { statName:statName, statValue:statValue }}
};
export const setStatUsefulArtifact = (stat, useful) => {
    var statName = STATS.find(elem => elem.display_name === stat).name;
    return {type: SET_STAT_USEFUL_ARTIFACT, payload: { statName:statName, useful:useful }}
};