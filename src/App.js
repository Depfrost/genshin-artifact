import Application from './containers/index'
import './App.css';

function App() {
  return (
    <Application/>
  );
}

export default App;
