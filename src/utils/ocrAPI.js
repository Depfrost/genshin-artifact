import axios from 'axios'
const settings = require('../settings.json')

export const postCallApi = (route, obj, config, isCredential) => {
    if (isCredential === true) {
        axios.defaults.withCredentials = true
    }
    return axios.post(settings.ocr_api + route, obj, config)
}

export const getCallApi = (route, obj, isCredential) => {
    if (isCredential === true) {
        axios.defaults.withCredentials = true
    }
    return axios.get(settings.ocr_api + route, obj)
}

export const deleteCallApi = (route, isCredential) => {
    if (isCredential === true) {
        axios.defaults.withCredentials = true
    }
    return axios.delete(settings.ocr_api + route)
}