import { notification } from 'antd'

export const errorNotification = (title, description) => {
    notification.error({
        message: title,
        description: description
    })
}

export const successNotification = (title, description) => {
    notification.success({
        message: title,
        description: description
    })
}