import axios from 'axios'
const settings = require('../settings.json')

export const postCallApi = (route, obj, isCredential) => {
    if (isCredential === true) {
        axios.defaults.withCredentials = true
    }
    return axios.post(settings.genshin_artifact_api + route, obj)
}

export const getCallApi = (route, obj, isCredential) => {
    if (isCredential === true) {
        axios.defaults.withCredentials = true
    }
    return axios.get(settings.genshin_artifact_api + route, obj)
}

export const deleteCallApi = (route, isCredential) => {
    if (isCredential === true) {
        axios.defaults.withCredentials = true
    }
    return axios.delete(settings.genshin_artifact_api + route)
}