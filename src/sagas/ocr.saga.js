import { call, put, takeLatest, select } from 'redux-saga/effects'
import { errorNotification } from '../utils/notification'
import { postCallApi } from '../utils/ocrAPI'
import * as constants from '../constants/artifact'
import settings from '../settings'
import { artifactSelector } from '../store/selectors'
import { STATS } from '../constants/artifact'
import fuzz from 'fuzzball'

const parse = (text) => {
    let res =  {
        level: 0,
        stats: []
    }
    let choices = STATS.map(e => e.display_name)
    let fuzzOptions = {
        scorer: fuzz.ratio,
        full_process: false, // prevent removing % from text
        limit: 1 // Only get the best matching result
    }

    text = text.split(/[\r\n]+/) // split text in lines

    text.forEach(line => {
        line = line.replace(':','.').replace('-','').replace('0/0','%') // Basic ocr errors replacement

        // Artifact level detection
        const levelVal = line.replace(' ', '').match(/^\+\d\d?$/)
        if (levelVal){
            res["level"] = parseInt(levelVal[0].replace('+', ''))
            return
        }

        // Artifact substats detection
        let newline =line.replace('+', '').replaceAll('.','').replace(/\d+/g, '') // Keep only the part relevant to substat detection
        let extract = fuzz.extract(newline, choices, fuzzOptions)[0]
        console.log(newline)
        console.log(extract)

        if (extract[1] > 80) {
            let values = line.replace(' ', '').match(/\d+(?:\.\d+)?/) // Get the value of the substat
            if (values){
                let stat = {}
                stat.name = STATS.find(e => e.display_name === extract[0]).name
                stat.value = Number(values[0])
                stat.useful = true
                stat.valid = false
                stat.tier = "C"
                stat.rolls = []
                res.stats.push(stat)
            }
        }
    });
    return res
}

function* requestOcrArtifact(action) {
    try
    {
        var formData = new FormData()
        formData.append("file", action.payload.image, action.payload.image.name)
        formData.append("apikey", settings.apikey)
        formData.append("OCREngine", 2)
        formData.append("language", "eng")
        formData.append("filetype", "PNG")
        const config = {
            headers: {
                "Content-Type": action.payload.image.type
            }
        }
        const result = yield call(postCallApi, '', formData, config)

        if (result.data.OCRExitCode !== 1 || !("ParsedResults" in result.data)){
            throw new Error("OCR error code:" + result.data.OCRExitCode)
        }
        const payload = parse(result.data.ParsedResults[0].ParsedText)
        yield put({type:constants.REQUEST_OCR_ARTIFACT_SUCCESS, payload: payload})
        
        const artifact = yield select(artifactSelector)
        yield put({type:constants.REQUEST_PROCESS_ARTIFACT, payload: artifact})
    }
    catch(e)
    {
        errorNotification('REQUEST OCR ARTIFACT', e.message)
        console.log(e)
    }
}

export default function* artifactSaga() {
    yield takeLatest(constants.REQUEST_OCR_ARTIFACT, requestOcrArtifact)
}