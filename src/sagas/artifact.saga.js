import { call, put, takeLatest, select } from 'redux-saga/effects'
import { errorNotification } from '../utils/notification'
import { getCallApi } from '../utils/backendAPI'
import * as constants from '../constants/artifact'
import { artifactSelector } from '../store/selectors'

function* processArtifact(action) {
    try
    {
        const artifact = yield select(artifactSelector)
        const obj = {
            params: {
                artifact: JSON.stringify(artifact)
            }
        }
        const newArtifact = yield call(getCallApi, '', obj)
        yield put({type:constants.REQUEST_PROCESS_ARTIFACT_SUCCESS, payload: newArtifact.data})
    }
    catch(e)
    {
        errorNotification('PROCESS ARTIFACT', e.message)
        console.log(e)
    }
}

export default function* artifactSaga() {
    yield takeLatest(constants.REQUEST_PROCESS_ARTIFACT, processArtifact)
    yield takeLatest(constants.SET_STARS_ARTIFACT, processArtifact)
    yield takeLatest(constants.SET_LEVEL_ARTIFACT, processArtifact)
    yield takeLatest(constants.SET_STATS_ARTIFACT, processArtifact)
    yield takeLatest(constants.SET_STAT_VALUE_ARTIFACT, processArtifact)
    yield takeLatest(constants.SET_STAT_USEFUL_ARTIFACT, processArtifact)
}