import { fork, all } from 'redux-saga/effects'
import artifactSaga from './artifact.saga'
import ocrSaga from './ocr.saga'

const sagas = [
    artifactSaga,
    ocrSaga
]

function* rootSaga() {
    yield all(sagas.map(saga => fork(saga)))
}

export default rootSaga