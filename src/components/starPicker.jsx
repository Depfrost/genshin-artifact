import React from 'react';
import { Radio } from 'antd';
import styles from '../styles/starPicker.module.scss'

function StarPicker ({ stars, setStars}) {
    const onChange = e => {
        setStars(e.target.value);
    };
    return (
        <div className={styles.container}>
            <span className={styles.title}>Stars</span>
            <Radio.Group onChange={onChange} value={stars} buttonStyle="solid" size="large">
                <Radio.Button value={4}>4 <i className={`fa fa-star ${styles.starColorFour}`}/></Radio.Button>
                <Radio.Button value={5}>5 <i className={`fa fa-star ${styles.starColorFive}`}/></Radio.Button>
            </Radio.Group>
        </div>
    );
}

export default StarPicker