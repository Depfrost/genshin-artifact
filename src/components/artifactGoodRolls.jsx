import React from 'react';
import styles from '../styles/artifactGoodRolls.module.scss'

function ArtifactGoodRolls ({ numberOfGoodRolls }) {
    var s = numberOfGoodRolls > 1 ? "s" : ""
    return (
    <span className={styles.text}>{numberOfGoodRolls} useful roll{s}: </span>
    );
}

export default ArtifactGoodRolls