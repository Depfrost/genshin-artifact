import React from 'react';
import styles from '../styles/artifactSelectorContainer.module.scss'

function ArtifactSelectorContainer ({ stars, imagePicker:ImagePicker, starPicker: StarPicker, levelPicker: LevelPicker, statPicker: StatPicker, statValuesPicker: StatValuesPicker }) {
    var containerClasses = stars === 4 ? `${styles.container} ${styles.four}` : `${styles.container} ${styles.five}`
    return (
        <React.Fragment>
            <div className={containerClasses}>
                <span className={styles.title}>Artifact</span>
                <ImagePicker/>
                <div className={styles.subcontainer}>
                    <StarPicker/>
                    <LevelPicker/>
                </div>
                <StatPicker/>
                <StatValuesPicker/>
            </div>
        </React.Fragment>
    );
}

export default ArtifactSelectorContainer