import React from 'react';
import styles from '../styles/artifactTier.module.scss'

function ArtifactTier ({ artifactTier }) {
    var tierToStyle = (tier) => {
        switch (tier) {
            case 'S':
                return styles.colorTierS
            case 'A':
                return styles.colorTierA
            case 'B':
                return styles.colorTierB
            case 'C':
                return styles.colorTierC
            default:
                return styles.colorTierC
          }
    }
    return (
        <span className={styles.text}>Your artifact is tier: <span className={`${styles.textSize} ${tierToStyle(artifactTier)}`}>{artifactTier}</span></span>
    );
}

export default ArtifactTier