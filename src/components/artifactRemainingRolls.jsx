import React from 'react';
import styles from '../styles/artifactRemainingRolls.module.scss'

function ArtifactRemainingRolls ({ numberOfRemainingRolls }) {
    var s = numberOfRemainingRolls > 1 ? "s" : ""
    return (
    <span className={styles.text}>{numberOfRemainingRolls} potential remaining roll{s}.</span>
    );
}

export default ArtifactRemainingRolls