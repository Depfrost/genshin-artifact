import React from 'react';
import StatDisplay from './statDisplay'
import styles from '../styles/artifactResume.module.scss'

function ArtifactResume ({ artifactValid, stats, artifactTier: ArtifactTier, artifactGoodRolls : ArtifactGoodRolls, artifactBadRolls : ArtifactBadRolls, artifactRemainingRolls : ArtifactRemainingRolls }) {
    var usefulStats = stats.filter((stat) => {
        return stat.useful && stat.valid
    });
    var uselessStats = stats.filter((stat) => {
        return !stat.useful && stat.valid
    });
    var noUsefulStats = usefulStats.length === 0
    var noUselessStats = uselessStats.length === 0
    return (
        <div className={styles.container}>
            {
                artifactValid ? <ArtifactTier/> : <span className={styles.title}>Artifact is invalid</span>
            }
            {
                !noUsefulStats && <span className={styles.goodRolls}><ArtifactGoodRolls/></span>
            }
            {
                usefulStats.map((stat) => <StatDisplay key={stat.name} stat={stat}/>)
            }
            <br/>
            {
                !noUselessStats && <span className={styles.badRolls}><ArtifactBadRolls/></span>
            }
            {
                uselessStats.map((stat) => <StatDisplay key={stat.name} stat={stat}/>)
            }
            <br/>
            {
                artifactValid && <span className={styles.remainingRolls}><ArtifactRemainingRolls/></span> 
            }
        </div>
    );
}

export default ArtifactResume