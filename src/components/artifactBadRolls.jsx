import React from 'react';
import styles from '../styles/artifactBadRolls.module.scss'

function ArtifactBadRolls ({ numberOfBadRolls }) {
    var s = numberOfBadRolls > 1 ? "s" : ""
    return (
    <span className={styles.text}>{numberOfBadRolls} useless roll{s}:</span>
    );
}

export default ArtifactBadRolls