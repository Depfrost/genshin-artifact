import React from 'react';
import { Radio } from 'antd';
import styles from '../styles/levelPicker.module.scss'

function LevelPicker ({ level, setLevel}) {
    const onChange = e => {
        setLevel(e.target.value);
    };
    return (
        <div className={styles.container}>
            <span className={styles.title}>Level</span>
            <Radio.Group onChange={onChange} value={level} buttonStyle="solid" size="large">
                <Radio.Button value={0}>0</Radio.Button>
                <Radio.Button value={4}>4</Radio.Button>
                <Radio.Button value={8}>8</Radio.Button>
                <Radio.Button value={12}>12</Radio.Button>
                <Radio.Button value={16}>16</Radio.Button>
                <Radio.Button value={20}>20</Radio.Button>
            </Radio.Group>
        </div>
    );
}

export default LevelPicker