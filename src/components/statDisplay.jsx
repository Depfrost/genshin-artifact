import React from 'react';
import styles from '../styles/statDisplay.module.scss'

function StatDisplay ({ stat }) {
    var s = stat.rolls.length > 1 ? "s" : ""
    var tierToStyle = (tier) => {
        switch (tier) {
            case 'S':
                return styles.colorTierS
            case 'A':
                return styles.colorTierA
            case 'B':
                return styles.colorTierB
            case 'C':
                return styles.colorTierC
            default:
                return styles.colorTierC
          }
    }
    return (
        stat.valid &&
        <div key={stat.name} className={styles.subcontainer}>
            <span className={styles.statName}>{stat.name}</span>
            <span className={styles.statValue}>{stat.value}</span>
            <span className={styles.statRollsText}>{stat.rolls.length} roll{s}</span>
            <span className={styles.statRolls}>
            {
                stat.rolls.map((element, i, arr) => {
                    if (arr.length - 1 === i) {
                        return <span key={i} className={tierToStyle(element.tier)}>{element.value}</span>
                    } else {
                        return <span key={i}><span className={tierToStyle(element.tier)}>{element.value}</span><span className={styles.textColor}> + </span></span>
                    }
                })
            }
            </span>
            <span className={`${styles.statTier} ${tierToStyle(stat.tier)}`}>{stat.tier}</span>
        </div>
    );
}

export default StatDisplay