import React from 'react'
import { Checkbox, InputNumber } from 'antd';
import styles from '../styles/statValuesPicker.module.scss'


function StatValuesPicker ({ stats, setStatValue, setStatUseful}) {
    const onStatChange = (stat, value) => {
        setStatValue(stat, Number(value));
    };
    const onStatUsefulChange = (stat, e) => {
        setStatUseful(stat, e.target.checked);
    };

    return (
        <div className={styles.container}>
            {
                stats.map(stat => {
                    var classInput = stat.valid ? '' : styles.inputInvalid
                    return (
                        <div key={stat.name} className={styles.statSelectorContainer}>
                            <span className={styles.statStyle}>{stat.name}</span>
                            <InputNumber min={0} max={10000} value={stat.value} defaultValue={stat.value} onChange={(e) => onStatChange(stat.name, e)} size="large"  className={classInput}/>
                            <Checkbox checked={stat.useful} onChange={(e) => onStatUsefulChange(stat.name, e)} className={styles.color}>Usefull</Checkbox>
                        </div>
                    )
                })
            }
        </div>
    );
}
    

export default StatValuesPicker