import React from 'react'
import { Select } from 'antd';
import { STATS } from '../constants/artifact'
import styles from '../styles/statPicker.module.scss'

const OPTIONS = STATS.map(elem => elem.display_name);

function StatPicker ({ stats, setStats}) {
    var filteredOptions = (stats.length < 4) ? OPTIONS.filter(o => !stats.includes(o)) : []

    const handleChange = stats => {
        setStats(stats);
    };

    return (
        <div className={styles.container}>
            <span className={styles.title}>Stats</span>
            <Select
                className={styles.input}
                mode="multiple"
                placeholder="Pick your stats"
                value={stats}
                onChange={handleChange}
                allowClear={true}
                style={{ width: '100%' }}
                size="large"
            >
                {filteredOptions.map(item => (
                    <Select.Option key={item} value={item}>
                        {item}
                    </Select.Option>
                ))}
            </Select>
        </div>
    );
}
    

export default StatPicker