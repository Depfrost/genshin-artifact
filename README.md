# genshin-artifact

This project allow to determine the rolls and tier of an artifact in the game Genshin Impact.
It requires [this](https://gitlab.com/Depfrost/genshin-artifact-api) back-end server to be up and running.

The project is also hosted [here](https://genshin-artifact-prod.herokuapp.com/) on Heroku.

## How to use

You can either enter all the artifact stats manually, or just paste a screenshot from the clipboard (click on the image then Ctrl-v).
Thanks to OCR the stats will be automatically parsed (excluding stars and level). 

Example of a good image :
![exampleArtifact](https://cdn.discordapp.com/attachments/789266305030422562/793287566992080926/unknown.png)